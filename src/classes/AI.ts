import Papa from 'papaparse';

import { Direction, Outcome } from '../types';
import Position from './Position';

type Move = [number, Direction, Outcome, number];
type Moves = { [position: string]: Move };

/*
    T^(-1)(p)    | ↙   ↓   ↘   ←   →   ↖   ↑   ↗
    -------+--------------------------------
    Sr(p)        | ↖   ↑   ↗   ←   →   ↙   ↓   ↘
    I(Sr(p))     | ↖   ←   ↙   ↑   ↓   ↗   →   ↘
    I(Sr(Sc(p))) | ↗   →   ↘   ↑   ↓   ↖   ←   ↙
    Sr(Sc(p))    | ↗   ↑   ↖   →   ←   ↘   ↓   ↙
    Sc(p)        | ↘   ↓   ↙   →   ←   ↗   ↑   ↖
    I(Sc(p))     | ↘   →   ↗   ↓   ↑   ↙   ←   ↖
    I(p)         | ↙   ←   ↖   ↓   ↑   ↘   →   ↗
    p            | ↙   ↓   ↘   ←   →   ↖   ↑   ↗

    For a given position p.
    When we apply the transformation T, we get q=T(p).
    Then, when we compute the best direction for q, we
    need to know the corresponding direction for p.
    So we use the following matrix M which gives the
    best direction of p = T^(-1)(q).

    The values are nulls or directions (enum), but we use the
    numeric values instead of the enum keys for readability.
*/
export const DIRECTION_TRANSFORMATIONS = [
  [null, 7, 8, 9, 4, null, 6, 1, 2, 3],
  [null, 7, 4, 1, 8, null, 2, 9, 6, 3],
  [null, 9, 6, 3, 8, null, 2, 7, 4, 1],
  [null, 9, 8, 7, 6, null, 4, 3, 2, 1],
  [null, 3, 2, 1, 6, null, 4, 9, 8, 7],
  [null, 3, 6, 9, 2, null, 8, 1, 4, 7],
  [null, 1, 4, 7, 2, null, 8, 3, 6, 9],
  [null, 1, 2, 3, 4, null, 6, 7, 8, 9],
];

export default class AI {
  private static moves: Moves = {};

  public static init(): Promise<void> {
    return new Promise((resolve: Function) => {
      Papa.parse('https://g-dv.gitlab.io/neutreeko/data/moves.csv', {
        download: true,
        fastMode: true,
        worker: true,
        step: ({ data }) => {
          const [position, pawn, direction, outcome, nTurns] = data;
          AI.moves[position as string] = [
            Number(pawn),
            direction,
            outcome,
            Number(nTurns),
          ];
        },
        complete: () => {
          resolve();
        },
      });
    });
  }

  public static play(position: Position, player: number): Position {
    const [id, noTransformation, order] = position.getId(player);
    const move = AI.moves[id];
    if (!move) {
      // it's a draw
      for (const noPawn of [0, 1, 2]) {
        for (const direction of [1, 2, 3, 4, 6, 7, 8, 9]) {
          // try the move
          const result = position.play(player, noPawn, direction as Direction);
          // if the move is valid..
          if (result.toString() !== position.toString()) {
            // ..and if it doesn't result in a losing position..
            if (!AI.moves[result.getId(1 - player)[0]]) {
              // ..then play it
              return result;
            }
          }
        }
      }
    }
    // it's not a draw
    const pawn = order[move[0]]; // the pawns have been re-ordered so we use its previous index
    const direction = DIRECTION_TRANSFORMATIONS[noTransformation][move[1]];
    return position.play(player, pawn, direction as Direction);
  }

  public static getOutcome(position: Position, player: number): Outcome {
    const id = position.getId(player)[0];
    return AI.moves[id] ? AI.moves[id][2] : Outcome.Draw;
  }
}
