/**
 * Classe encapsulant les opérations possibles sur le plateau.
 */
public class BoardOperations {

	private int[][] board_;

	BoardOperations(int[][] board) {
		board_ = board;
	}

	/**
	 * Affiche le plateau de jeu.
	 */
	public void displayBoard() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (board_[i][j] != 0)
					System.out.print(board_[i][j] + " ");
				else
					System.out.print("- ");
			}
			System.out.println("");
		}
		System.out.println("");
	}

	/**
	 * Inverse le contenu de deux cases du terrain.
	 * 
	 * @param a coordonnées de la première case
	 * @param b coordonnées de la deuxième case
	 */
	public void swap(Coords a, Coords b) {
		int tmp = board_[a.i][a.j];
		board_[a.i][a.j] = board_[b.i][b.j];
		board_[b.i][b.j] = tmp;
	}

	/**
	 * Détermine s'il est possible de bouger un pion dans une direction.
	 * 
	 * @param move déplacement à effectuer
	 * @return boolean indiquant si le déplacement serait possible ou non
	 */
	public boolean isValidMove(Move move) {
		Coords newPosition = move.position.add(move.direction);
		if (newPosition.isOnBoard())
			return board_[newPosition.i][newPosition.j] == 0;
		return false;
	}

}
