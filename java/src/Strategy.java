/**
 * Stratégie à appliquer sur un ensemble de coups.
 */
public abstract class Strategy {

    // Tour sur lequel on applique la stratégie.
    public Move currentMove_;

    /**
     * Exécute la stratégie.
     */
    public abstract int run();

    /**
     * Indique le tour sur lequel on applique la stratégie.
     */
    public void setCurrentMove(Move currentMove) {
        currentMove_ = currentMove;
    }

    /**
     * Indique s'il est inutile de continuer d'appliquer cette stratégie.
     */
    public boolean shouldExitNow() {
        return false;
    }

}
