import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Board from './components/Board';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <div className="container">
      <div className="row justify-content-center align-items-center">
        <div className="card shadow-sm">
          <div className="card-body">
            <Board />
            <div className="text-right">
              <a href="https://gitlab.com/g-dv/neutreeko/-/blob/master/README.md#rules">
                Rules
                <i className="ml-1 mr-4 fa fa-info-circle" />
              </a>
              <a href="https://gitlab.com/g-dv/neutreeko">
                Repo
                <i className="ml-1 mr-4 fab fa-gitlab" />
              </a>
              <a href="https://www.neutreeko.net/neutreeko.htm">
                Website
                <i className="ml-1 fa fa-globe" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
