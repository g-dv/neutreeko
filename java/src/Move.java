/**
 * Représente un déplacement.
 */
public class Move {

    // ...initiale
    public Coords position;

    // ...dans laquelle aller
    public Coords direction;

    Move(Coords position, Coords direction) {
        this.position = position;
        this.direction = direction;
    }

}
