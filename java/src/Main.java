/**
 * Neutreeko
 * 
 * @description Ce programme modélise de façon triviale le jeu Neutreeko dans la
 *              console. Les règles du jeu sont disponibles à l'url suivante :
 *              http://www.neutreeko.net/neutreeko.htm. Le jeu permet de jouer
 *              contre l'ordinateur.
 * @author GDV
 * @date 11/10/2016
 */
public class Main {

	public static void main(String[] args) {

		// Affichage des instructions.
		System.out.println("INSTRUCTIONS");
		System.out.println("  Positions");
		System.out.println("    0 0  0 1  ...  0 4");
		System.out.println("    1 0  ...  ...  ...");
		System.out.println("    ...  ...  ...  ...");
		System.out.println("    4 0  ...  ...  4 4");
		System.out.println("  Directions");
		System.out.println("    7  8  9");
		System.out.println("    4     6");
		System.out.println("    1  2  3");
		System.out.println("  Entrée acceptée");
		System.out.println("    [0-4] [0-4] [1-46-9]");
		System.out.println("    $1 position.i");
		System.out.println("    $2 position.j");
		System.out.println("    $3 direction");
		System.out.println("  Exemple");
		System.out.println("    (0,1) droite <=> 0 3 6\n");

		// Lancement.
		AIGame game = new AIGame();
		game.start();

	}

}
