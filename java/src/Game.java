import java.util.Scanner;

/**
 * Classe principale de l'application. Représente une partie.
 */
public abstract class Game {

	// Plateau de jeu. Contient 0 si la case est vide. 1 ou 2 sinon.
	protected int[][] board_ = new int[5][5];

	// Opérations sur le plateau.
	protected BoardOperations boardOperations_ = new BoardOperations(board_);

	// Coordonnées des pions. [, joueur1, joueur2][pion1, pion2, pion3].
	protected Coords[][] pawns_ = new Coords[3][3];

	// Joueur dont c'est le tour de jouer.
	protected int currentPlayer_ = 2;

	// 8 directions possibles (NO-N-NE-O-E-SO-S-SE) sous forme de coordonnées.
	protected final Coords[] directions_ = new Coords[8];

	Game() {
		// Position initiale des pions sur le plateau.
		board_[0][1] = board_[0][3] = board_[3][2] = 1;
		board_[1][2] = board_[4][1] = board_[4][3] = 2;
		pawns_[1][0] = new Coords(0, 1);
		pawns_[1][1] = new Coords(0, 3);
		pawns_[1][2] = new Coords(3, 2);
		pawns_[2][0] = new Coords(1, 2);
		pawns_[2][1] = new Coords(4, 1);
		pawns_[2][2] = new Coords(4, 3);

		// On remplit le vecteur des 8 directions possibles.
		int k = 0;
		for (int i = -1; i <= 1; i++)
			for (int j = -1; j <= 1; j++)
				if (i != 0 || j != 0)
					directions_[k++] = new Coords(i, j);
	}

	/**
	 * Détermine si le joueur a réussi à aligner ses 3 pions.
	 * 
	 * @param player joueur pour lequel on teste la victoire
	 * @return bool indiquant si le joueur a gagné ou non
	 */
	protected boolean hasWon(int player) {
		for (int i = 0; i < 3; i++) { // Chaque pion possible.
			for (Coords direction : directions_) { // Chaque direction.
				// On regarde si les deux cases suivantes dans cette direction
				// contiennent les deux autres pions.
				Coords square2 = pawns_[player][i].add(direction);
				Coords square3 = square2.add(direction);
				if (square3.isOnBoard()) // Si on est toujours dans le terrain.
					if (board_[square2.i][square2.j] == player && board_[square3.i][square3.j] == player)
						return true;
			}
		}
		return false;
	}

	/**
	 * Déplace un pion dans une certaine direction.
	 * 
	 * @param move déplacement à effectuer
	 * @return nouvelle position du pion post-déplacement
	 */
	protected Coords move(Move move) {
		// Joueur possédant le pion.
		Coords oldPosition = move.position;
		int player = board_[oldPosition.i][oldPosition.j];
		Coords newPosition = oldPosition.clone();
		// On déplace le pion dans la direction tant qu'on peut.
		while (boardOperations_.isValidMove(new Move(newPosition, move.direction)))
			newPosition.addAssign(move.direction);
		// On a trouvé la position finale. On y met le pion.
		boardOperations_.swap(newPosition, oldPosition);
		// On met à jour les coordonnées des pions du joueur.
		for (int i = 0; i < 3; i++) { // On cherche le bon pion.
			if (pawns_[player][i].equals(oldPosition)) { // C'est le bon pion.
				pawns_[player][i] = newPosition;
				break;
			}
		}
		return newPosition;
	}

	/**
	 * Lit l'entrée du joueur. Si elle est valide, effectue le déplacement.
	 * 
	 * @param sc scanner pour lire l'input
	 */
	protected void playUserMove(Scanner sc) {
		while (true) {

			// Saisie.
			int i = sc.nextInt();
			int j = sc.nextInt();
			int direction = sc.nextInt();

			// Validation de la direction et traduction en coordonnées.
			// direction devient un index du tableau directions_[8].
			boolean isValidDirection = true;
			if (direction >= 1 && direction <= 3)
				direction += 4;
			else if (direction == 4)
				direction--;
			else if (direction == 6)
				direction -= 2;
			else if (direction >= 7 && direction <= 9)
				direction -= 7;
			else
				isValidDirection = false;

			Coords position = new Coords(i, j);

			// Vérifications et déplacement.
			if (isValidDirection) {
				if (position.isOnBoard()) {
					if (board_[i][j] == currentPlayer_) {
						Move move = new Move(position, directions_[direction]);
						if (boardOperations_.isValidMove(move)) {
							move(move);
							return;
						} else
							System.out.println("Direction invalide");
					} else
						System.out.println("Aucun pion disponible");
				} else
					System.out.println("Position invalide");
			} else
				System.out.println("Direction invalide");
		}

	}

	protected int getOpponent() {
		return (currentPlayer_ % 2) + 1;
	}

}
