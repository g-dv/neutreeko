/**
 * Coordonnées de cases du plateau.
 */
public class Coords {

	public int i;
	public int j;

	Coords(int i, int j) {
		this.i = i;
		this.j = j;
	}

	/**
	 * Remplace l'opérateur +.
	 */
	public Coords add(Coords coords) {
		return new Coords(i + coords.i, j + coords.j);
	}

	/**
	 * Remplace l'opérateur +=.
	 */
	public void addAssign(Coords coords) {
		i += coords.i;
		j += coords.j;
	}

	/**
	 * Renvoie une copie de l'objet.
	 */
	public Coords clone() {
		return new Coords(i, j);
	}

	/**
	 * Remplace l'opérateur ==.
	 */
	public boolean equals(Coords coords) {
		return i == coords.i && j == coords.j;
	}

	/**
	 * Vérifie si les coordonnées pointent sur une case du plateau.
	 */
	public boolean isOnBoard() {
		return i >= 0 && i < 5 && j >= 0 && j < 5;
	}

}
