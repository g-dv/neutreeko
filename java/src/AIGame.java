import java.util.Scanner;

/**
 * Cette "Game" introduit une IA avec un algorithme min-max.
 */
public class AIGame extends Game {

	/**
	 * Lance la partie.
	 */
	public void start() {
		// Initialisation.
		Scanner scanner = new Scanner(System.in);
		AI ai = new AI(this);

		// Boucle principale.
		do {
			// C'est au tour de l'autre joueur.
			currentPlayer_ = getOpponent();

			// Affichage.
			boardOperations_.displayBoard();
			System.out.println("Joueur " + currentPlayer_ + " : ");

			PlayerType player = (currentPlayer_ == 1) ? Settings.PLAYER_1 : Settings.PLAYER_2;
			if (player == PlayerType.AI) {
				ai.playBestMove();
			} else
				playUserMove(scanner);
		} while (!hasWon(currentPlayer_));

		// Fin du jeu.
		boardOperations_.displayBoard();
		System.out.println("Partie terminée. Victoire du joueur " + currentPlayer_ + ".");
		scanner.close();
	}

}
