/**
 * Paramètres du jeu.
 */
public final class Settings {

	/**
	 * Choix des deux adversaires.
	 * ie. La partie peut être multijoueur, humain-IA ou IA-IA.
	 */
	public static final PlayerType PLAYER_1 = PlayerType.HUMAN;
	public static final PlayerType PLAYER_2 = PlayerType.AI;

	/**
	 * Profondeur maximale de l'analyse récursive.
	 * ie. Nombre de coups que l'IA verra à l'avance.
	 */
	public static final int MAX_MINMAX_DEPTH = 7;

	/**
	 * Profondeur maximale du décompte récursif du nombre d'erreurs possibles.
	 * Le nombre doit être inférieur ou égal à la profondeur de l'évaluation.
	 * ie. L'IA comptera le nombre d'erreurs fatales que l'adversaire peut
	 * faire au prochain tour.
	 */
	public static final int MAX_OPENINGS_DEPTH = 4;

}
