export enum Direction {
  // The values correspond to the keys of a num. pad.
  // 5 if missing because it is at the center of the num. pad.
  SW = 1,
  S = 2,
  SE = 3,
  W = 4,
  E = 6,
  NW = 7,
  N = 8,
  NE = 9,
}

export type Transformation = [
  string, // resulting id
  number, // no of the transformation applied
  [number, number, number] // indices of the pawns before sorting
];

export enum Outcome {
  Victory = 'W',
  Defeat = 'L',
  Draw = 'D',
}
