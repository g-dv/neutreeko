import { Cell } from "./cell";

export class Position {
  private pawns: [Cell[], Cell[]];

  constructor(str: string) {
    this.pawns = [
      [
        new Cell().fromString(str.substr(0, 2)),
        new Cell().fromString(str.substr(2, 2)),
        new Cell().fromString(str.substr(4, 2)),
      ],
      [
        new Cell().fromString(str.substr(6, 2)),
        new Cell().fromString(str.substr(8, 2)),
        new Cell().fromString(str.substr(10, 2)),
      ],
    ];
  }

  public toString(shouldSort = true): string {
    let str = "";
    for (const player of [0, 1]) {
      const cells = this.pawns[player].map((cell) => cell.toString());
      if (shouldSort) cells.sort();
      str += cells.join("");
    }
    return str;
  }

  public getEquivalentStrings(shouldSortPosition = true): string[] {
    const position = new Position(this.toString());
    // symmetric transformation
    const s = (): void => {
      for (let player of [0, 1]) {
        for (let pawn of [0, 1, 2]) {
          position.pawns[player][pawn].row =
            6 - position.pawns[player][pawn].row;
        }
      }
    };
    // inverse rows and cols
    const i = (): void => {
      for (let player of [0, 1]) {
        for (let pawn of [0, 1, 2]) {
          let tmp = position.pawns[player][pawn].row;
          position.pawns[player][pawn].row = position.pawns[player][pawn].col;
          position.pawns[player][pawn].col = tmp;
        }
      }
    };

    // by chaining these transformations, we obtain the 8 equivalent positions
    // #    TRANS. POSITION     ROW     COL
    // 1.          p            r       c
    // 2.   Sr     Sr(p)        6-r     c
    // 3.   I      I(Sr(p))     c       6-r
    // 4.   Sr     I(Sr(Sc(p))) 6-c     6-r
    // 5.   I      Sr(Sc(p))    6-r     6-c
    // 6.   Sr     Sc(p)        r       6-c
    // 7.   I      I(Sc(p))     6-c     r
    // 8.   Sr     I(p)         c       r
    let strings = [];
    for (const transformPosition of [s, i, s, i, s, i, s, i]) {
      transformPosition();
      strings.push(position.toString(shouldSortPosition));
    }
    return strings;
  }

  public getId(): string {
    const strings = this.getEquivalentStrings();
    strings.sort();
    return strings[0];
  }

  public hasWon(player: number): boolean {
    const pawns = this.pawns[player];
    if (
      pawns[0].col === pawns[1].col - 1 &&
      pawns[0].col === pawns[2].col - 2
    ) {
      // same row
      if (pawns[0].row === pawns[1].row && pawns[0].row === pawns[2].row) {
        return true;
      }
      // diagonal 1
      if (
        pawns[0].row === pawns[1].row - 1 &&
        pawns[0].row === pawns[2].row - 2
      ) {
        return true;
      }
      // diagonal 2
      if (
        pawns[0].row === pawns[1].row + 1 &&
        pawns[0].row === pawns[2].row + 2
      ) {
        return true;
      }
    }
    // same column
    if (pawns[0].col === pawns[1].col && pawns[0].col === pawns[2].col) {
      if (
        pawns[0].row === pawns[1].row - 1 &&
        pawns[0].row === pawns[2].row - 2
      ) {
        return true;
      }
    }
    return false;
  }

  public move(noPawn: number, direction: string): void {
    const pawn = new Cell(this.pawns[0][noPawn].col, this.pawns[0][noPawn].row);
    if (direction === "SW") {
      while (
        pawn.col > 1 &&
        pawn.row > 1 &&
        this.isFree(pawn.col - 1, pawn.row - 1)
      ) {
        pawn.col--;
        pawn.row--;
      }
    }
    if (direction === "S") {
      while (pawn.row > 1 && this.isFree(pawn.col, pawn.row - 1)) {
        pawn.row--;
      }
    }
    if (direction === "SE") {
      while (
        pawn.col < 5 &&
        pawn.row > 1 &&
        this.isFree(pawn.col + 1, pawn.row - 1)
      ) {
        pawn.col++;
        pawn.row--;
      }
    }
    if (direction === "W") {
      while (pawn.col > 1 && this.isFree(pawn.col - 1, pawn.row)) {
        pawn.col--;
      }
    }
    if (direction === "E") {
      while (pawn.col < 5 && this.isFree(pawn.col + 1, pawn.row)) {
        pawn.col++;
      }
    }
    if (direction === "NW") {
      while (
        pawn.col > 1 &&
        pawn.row < 5 &&
        this.isFree(pawn.col - 1, pawn.row + 1)
      ) {
        pawn.col--;
        pawn.row++;
      }
    }
    if (direction === "N") {
      while (pawn.row < 5 && this.isFree(pawn.col, pawn.row + 1)) {
        pawn.row++;
      }
    }
    if (direction === "NE") {
      while (
        pawn.col < 5 &&
        pawn.row < 5 &&
        this.isFree(pawn.col + 1, pawn.row + 1)
      ) {
        pawn.col++;
        pawn.row++;
      }
    }
    this.pawns[0][noPawn] = pawn;
  }

  private isFree(col: number, row: number): boolean {
    for (const pawns of this.pawns) {
      for (const cell of pawns) {
        if (cell.col === col && cell.row === row) {
          return false;
        }
      }
    }
    return true;
  }
}
