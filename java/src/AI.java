public class AI {

	public AIGame game_;

	public AI(AIGame game) {
		game_ = game;
	}

	public void playBestMove() {
		// Initialisation.
		FindBestMoveStrategy strategy = new FindBestMoveStrategy();
		// On applique l'algorithme min-max.
		forEachPossibleMove(strategy, game_.currentPlayer_);
		// Interprétation.
		if (strategy.maxScore > 0)
			System.out.println("Je vais gagner...\n");
		else if (strategy.maxScore < 0)
			System.out.println("Tu peux gagner...\n");
		// Finalement, on effectue le meilleur coup.
		game_.move(strategy.bestMove);
	}

	private class FindBestMoveStrategy extends Strategy {

		public int maxScore = -1001;
		public Move bestMove = null;
		private int maxNOpenings = 0;

		public int run() {
			// Initialisation.
			MinMax minMax = new MinMax(AI.this);
			// On évalue le coup selon le critère victoire/défaite.
			int score = minMax.run();
			// On met à jour le meilleur coup possible.
			if (score >= maxScore) {
				// En cas d'égalité, on choisit un coup en visant à maximiser les possibilités
				// d'erreur de l'adversaire.
				int nOpenings = minMax.nOpenings_;
				if (score > maxScore || (score == maxScore && nOpenings > maxNOpenings)) {
					maxScore = score;
					bestMove = currentMove_;
					maxNOpenings = nOpenings;
				}
			}
			return -1;
		}

	}

	/**
	 * Exécute une stratégie pour tous les mouvements possibles.
	 */
	public int forEachPossibleMove(Strategy strategy, int player) {
		int res = 0;
		// Pour chaque pion.
		for (int i = 0; i < 3; i++) {
			// On enregistre la position.
			Coords oldPosition = game_.pawns_[player][i];
			// Pour chaque coup valide.
			for (Coords direction : game_.directions_) {
				Move move = new Move(oldPosition, direction);
				if (game_.boardOperations_.isValidMove(move)) {
					// On joue le coup.
					Coords newPosition = game_.move(move);
					strategy.setCurrentMove(move);
					// On exécute la stratégie.
					res = strategy.run();
					// On annule le coup.
					game_.pawns_[player][i] = oldPosition;
					game_.boardOperations_.swap(newPosition, oldPosition);
					// Éventuel raccourci alpha-beta.
					if (strategy.shouldExitNow())
						return res;
				}
			}
		}
		return res;
	}

}
