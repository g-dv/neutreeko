/**
 * Type de joueur.
 */
public enum PlayerType {
	AI, HUMAN;
}
