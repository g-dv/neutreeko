/**
 * Implémente un algorithme min-max pour éviter de perdre et gagner si c'est
 * possible.
 */

public class MinMax {

    protected AIGame game_;
    protected AI ai_;
    public int score_;
    public int nOpenings_;

    public MinMax(AI ai) {
        game_ = ai.game_;
        ai_ = ai;
    }

    /**
     * Porte d'entrée de l'algorithme.
     */
    public int run() {
        if (game_.hasWon(game_.currentPlayer_)) // On a déjà gagné.
            return 1000;
        StrategyForMin min = new StrategyForMin(1001, -1001, 0);
        score_ = ai_.forEachPossibleMove(min, game_.getOpponent());
        nOpenings_ = min.nOpenings_;
        return score_;
    }

    /**
     * Représente les stratégies de l'algorithme (min et max).
     */
    private abstract class MinMaxStrategy extends Strategy {

        protected int minScore_;
        protected int maxScore_;
        protected int depth_;
        protected int player_;

        public MinMaxStrategy(int minScore, int maxScore, int depth) {
            minScore_ = minScore;
            maxScore_ = maxScore;
            depth_ = depth;
        }

    }

    /**
     * Fonction max.
     */
    private class StrategyForMax extends MinMaxStrategy {

        public StrategyForMax(int minScore, int maxScore, int depth) {
            super(minScore, maxScore, depth);
            player_ = game_.getOpponent();
        }

        /**
         * Exécution.
         */
        public int run() {
            if (game_.hasWon(player_)) // L'adversaire a gagné.
                return (-1000 + depth_);
            if (depth_ == Settings.MAX_MINMAX_DEPTH) // Profondeur max atteinte.
                return 0;
            Strategy min = new StrategyForMin(minScore_, maxScore_, depth_ + 1);
            maxScore_ = Math.max(maxScore_, ai_.forEachPossibleMove(min, player_));
            return maxScore_;
        }

        /**
         * Coupure alpha-beta.
         */
        public boolean shouldExitNow() {
            return depth_ >= Settings.MAX_OPENINGS_DEPTH && maxScore_ >= minScore_;
        }

    }

    /**
     * Fonction min.
     */
    private class StrategyForMin extends MinMaxStrategy {

        public int nOpenings_ = 0;

        public StrategyForMin(int minScore, int maxScore, int depth) {
            super(minScore, maxScore, depth);
            player_ = game_.currentPlayer_;
        }

        /**
         * Exécution.
         */
        public int run() {
            if (game_.hasWon(player_)) // On a gagné.
                return 1000 - depth_;
            if (depth_ == Settings.MAX_MINMAX_DEPTH) // Profondeur max atteinte.
                return 0;
            Strategy max = new StrategyForMax(minScore_, maxScore_, depth_ + 1);
            int score = ai_.forEachPossibleMove(max, player_);
            if (depth_ == 0 && score > 0) {
                nOpenings_++;
            }
            minScore_ = Math.min(minScore_, score);
            return minScore_;
        }

        /**
         * Coupure alpha-beta.
         */
        public boolean shouldExitNow() {
            return depth_ >= Settings.MAX_OPENINGS_DEPTH && minScore_ <= maxScore_;
        }

    }

}
