import { Direction, Transformation } from '../types';
import Cell from './Cell';

type Pawns = [Cell, Cell, Cell];

export default class Position {
  private pawns: Pawns[];

  constructor(position?: Position, player = 0) {
    if (position) {
      // copy constructor
      this.pawns = position.pawns.map((group) =>
        group.map((cell) => cell.clone())
      ) as Pawns[];
      if (player) {
        this.pawns.push(this.pawns.shift() as Pawns);
      }
    } else {
      this.pawns = [
        [new Cell(2, 1), new Cell(3, 4), new Cell(4, 1)],
        [new Cell(2, 5), new Cell(3, 2), new Cell(4, 5)],
      ];
    }
  }

  public getPlayer(cell: Cell): number | undefined {
    for (const player of [0, 1]) {
      for (const pawn of this.pawns[player]) {
        if (pawn.equals(cell)) {
          return player;
        }
      }
    }
    return undefined;
  }

  public move(pawn: Cell, target: Cell) {
    for (const player of [0, 1]) {
      for (const noPawn of [0, 1, 2]) {
        if (this.pawns[player][noPawn].equals(pawn)) {
          this.pawns[player][noPawn] = target;
          return;
        }
      }
    }
  }

  private isFree(cell: Cell): boolean {
    for (const pawns of this.pawns) {
      for (const pawn of pawns) {
        if (pawn.equals(cell)) {
          return false;
        }
      }
    }
    return true;
  }

  public hasWon(player: number): boolean {
    // sort by col and row
    const [p1, p2, p3] = this.pawns[player].slice().sort((a, b) => {
      return a.col * 10 + a.row - b.col * 10 - b.row;
    });
    if (p1.col === p2.col - 1 && p1.col === p3.col - 2) {
      return (
        (p1.row === p2.row && p1.row === p3.row) || // same row
        (p1.row === p2.row - 1 && p1.row === p3.row - 2) || // diagonal 1
        (p1.row === p2.row + 1 && p1.row === p3.row + 2) // diagonal 2
      );
    }
    // same column
    return (
      p1.col === p2.col &&
      p1.col === p3.col &&
      p1.row === p2.row - 1 &&
      p1.row === p3.row - 2
    );
  }

  public getDistanceForMove(pawn: Cell, dCol: number, dRow: number): number {
    let i: number;
    for (
      i = 1;
      pawn.col + i * dCol >= 1 &&
      pawn.row + i * dRow >= 1 &&
      pawn.col + i * dCol <= 5 &&
      pawn.row + i * dRow <= 5 &&
      this.isFree(new Cell(pawn.col + i * dCol, pawn.row + i * dRow));
      i++
    );
    return i - 1;
  }

  public getReachableCells(pawn: Cell): Cell[] {
    const reachableCells = [] as Cell[];
    const addForDirection = (dCol: number, dRow: number) => {
      const nCells = this.getDistanceForMove(pawn, dCol, dRow);
      if (nCells) {
        reachableCells.push(
          new Cell(pawn.col + nCells * dCol, pawn.row + nCells * dRow)
        );
      }
    };
    // Direction: ↙
    addForDirection(-1, -1);
    // Direction: ↓
    addForDirection(0, -1);
    // Direction: ↘
    addForDirection(+1, -1);
    // Direction: ←
    addForDirection(-1, 0);
    // Direction: →
    addForDirection(+1, 0);
    // Direction: ↖
    addForDirection(-1, +1);
    // Direction: ↑
    addForDirection(0, +1);
    // Direction: ↗
    addForDirection(+1, +1);

    return reachableCells;
  }

  public toString(): string {
    return this.pawns.map((p) => p.map((c) => c.toString()).join('')).join('');
  }

  public getEquivalentStrings(currentPlayer: number): string[] {
    const position = new Position(this, currentPlayer);
    // symmetric transformation
    const s = (): void => {
      for (const player of [0, 1]) {
        for (const pawn of [0, 1, 2]) {
          position.pawns[player][pawn].row =
            6 - position.pawns[player][pawn].row;
        }
      }
    };
    // inverse rows and cols
    const i = (): void => {
      for (const player of [0, 1]) {
        for (const pawn of [0, 1, 2]) {
          const tmp = position.pawns[player][pawn].row;
          position.pawns[player][pawn].row = position.pawns[player][pawn].col;
          position.pawns[player][pawn].col = tmp;
        }
      }
    };

    // by chaining these transformations, we obtain the 8 equivalent positions
    // #    TRANS. POSITION     ROW     COL
    // 1.          p            r       c
    // 2.   Sr     Sr(p)        6-r     c
    // 3.   I      I(Sr(p))     c       6-r
    // 4.   Sr     I(Sr(Sc(p))) 6-c     6-r
    // 5.   I      Sr(Sc(p))    6-r     6-c
    // 6.   Sr     Sc(p)        r       6-c
    // 7.   I      I(Sc(p))     6-c     r
    // 8.   Sr     I(p)         c       r
    const ids = [];
    for (const transformPosition of [s, i, s, i, s, i, s, i]) {
      transformPosition();
      ids.push(position.toString());
    }
    return ids;
  }

  public getId(player: number): Transformation {
    const cellToLetter = (cell: string) =>
      String.fromCharCode(
        65 + 5 * (cell.charCodeAt(0) - 65) + Number(cell[1]) - 1
      );
    const stringToId = (str: string) =>
      cellToLetter(str.substr(0, 2)) +
      cellToLetter(str.substr(2, 2)) +
      cellToLetter(str.substr(4, 2)) +
      cellToLetter(str.substr(6, 2)) +
      cellToLetter(str.substr(8, 2)) +
      cellToLetter(str.substr(10, 2));
    let id = 'Z';
    let noTransformation = -1;
    let order: [number, number, number] = [0, 0, 0];
    const equivalentStrings = this.getEquivalentStrings(player);
    for (let i = 0; i < 8; i++) {
      const str = equivalentStrings[i];
      const cells = [
        [str.substr(0, 2), str.substr(2, 2), str.substr(4, 2)],
        [str.substr(6, 2), str.substr(8, 2), str.substr(10, 2)],
      ];
      const previousCells = [
        [str.substr(0, 2), str.substr(2, 2), str.substr(4, 2)],
        [str.substr(6, 2), str.substr(8, 2), str.substr(10, 2)],
      ];
      cells[0].sort();
      cells[1].sort();
      const sortedString = cells[0].join('') + cells[1].join('');
      if (sortedString < id) {
        id = sortedString;
        noTransformation = i;
        order = [
          previousCells[0].indexOf(cells[0][0]),
          previousCells[0].indexOf(cells[0][1]),
          previousCells[0].indexOf(cells[0][2]),
        ];
      }
    }
    return [stringToId(id), noTransformation, order];
  }

  public play(player: number, noPawn: number, direction: Direction): Position {
    const result = new Position(this);
    const pawn = result.pawns[player][noPawn];
    const moveInDirection = ([dCol, dRow]: [number, number]) => {
      const nCells = result.getDistanceForMove(pawn, dCol, dRow);
      result.pawns[player][noPawn] = new Cell(
        pawn.col + nCells * dCol,
        pawn.row + nCells * dRow
      );
    };
    const directions: [number, number][] = [
      [0, 0], // undefined direction
      [-1, -1],
      [0, -1],
      [1, -1],
      [-1, 0],
      [0, 0], // undefined direction
      [1, 0],
      [-1, 1],
      [0, 1],
      [1, 1],
    ];
    moveInDirection(directions[direction]);
    return result;
  }
}
