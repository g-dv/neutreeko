import { combinations } from "./combinations";
import { Position } from "./position";

////////////////////////////////////
//
// This script will print a csv that describes every winning strategy
//
////////////////////////////////////
// The goal is to compute all the winning/losing positions
// with some sort of dynamic programming.
// For a given position :
//   - if the position's id is A1A2A3B1B2B3, it means that player 0 has the
//       pawns A1, A2 and A3 and that player 1 has the pawns B1, B2 ans B3
//   - we always consider that it's player 0's turn
//       if we want to know what player 1's score would be if it was his turn,
//       we can just look the score for the position B1B2B3A1A2A3
//   - when writing the csv file, we reduce the number of characters by mapping
//       each cell to a letter (A1 -> A, A2 -> B, ... E4 -> X, E5 -> Y)
// We keep a dictionary with the score for each position.
//   - if score = MAX_DEPTH: player 0 won
//   - if score > 0: score = MAX_DEPTH - n means that player 0 wins in n turns
//   - if score < 0: score = n - MAX_DEPTH means that player 1 wins in n turns
//   - if score = 0: it should be a draw
// This dictionary is initialized with the positions where a player has won.
// Then, by looking at the positions that can immediately reach a winning state,
//   we have the positions that can win in one move
// And by repeating this process multiple times until we can't update the
//   dictionary anymore, we end up finding all the positions where one player
//   has a winning startegy.

const MAX_DEPTH = 100000; // a winning situation will certainly not require this many moves

type Direction = "SW" | "S" | "SE" | "W" | "E" | "NW" | "N" | "NE";
type Move = [string, number, Direction];

function generateIds(): string[] {
  // A1, A2, ... E5
  const getCells = (): string[] => {
    let cells: string[] = [];
    "ABCDE"
      .split("")
      .forEach((x) => cells.push(...[1, 2, 3, 4, 5].map((y) => x + y)));
    return cells;
  };
  // accumulate the ids
  const ids = [];
  for (const player1 of combinations(getCells(), 3)) {
    let difference = getCells().filter((x) => !player1.includes(x));
    for (const player2 of combinations(difference, 3)) {
      const str = player1.join("") + player2.join("");
      const position = new Position(str);
      if (
        position.getId() === str && // only consider one of the 8 equivalent positions
        (!position.hasWon(0) || !position.hasWon(1)) // if both players win, it's invalid
      ) {
        ids.push(str);
      }
    }
  }
  return ids;
}

function getMoves(id: string): Move[] {
  // we are looking for the positions that can reach the one given as argument
  const neighbors: Move[] = [];
  for (const pawn of [0, 1, 2]) {
    for (const direction of ["SW", "S", "SE", "W", "E", "NW", "N", "NE"]) {
      const position = new Position(id);
      position.move(pawn, direction);
      if (position.toString() !== id) {
        neighbors.push([position.getId(), pawn, direction as Direction]);
      }
    }
  }
  return neighbors;
}

function switchPlayers(id: string): string {
  // simply transform A1A2A3B1B2B3 into B1B2B3A1A2A3
  return new Position(id.substring(6) + id.substring(0, 6)).getId();
}

// CSV header
console.log("position,pawn,direction,outcome,nTurns");

// Finding the ids
const ids = generateIds();

// Initialize the dictionary
const scores: { [id: string]: number } = {};
ids.forEach((id) => (scores[id] = 0));

// First round: set the winning positions
let stack = [];
for (const id in scores) {
  if (new Position(id).hasWon(0)) {
    scores[id] = MAX_DEPTH;
  } else if (new Position(id).hasWon(1)) {
    scores[id] = -MAX_DEPTH;
  } else {
    stack.push(id);
  }
}

let previousLength = 0;
// exit if all resulting positions are draws (ie. we cannot find anything anymore)
while (previousLength !== stack.length) {
  previousLength = stack.length;
  // the new stack containing the positions for which we find nothing yet
  const newStack: string[] = [];
  for (const id of stack) {
    let min = Infinity;
    let bestMove: Move;
    // for each move that the player can do
    for (const move of getMoves(id)) {
      const neighborAsOpponent = switchPlayers(move[0]);
      // see if the move results in a position that we evaluated already
      if (scores[neighborAsOpponent] < min) {
        // the move results in a better position than the one we found
        min = scores[neighborAsOpponent];
        bestMove = move;
      }
    }
    // we are still finding a draw for this position
    if (min === 0) {
      newStack.push(id);
      continue;
    }
    // the position does not result in a draw!
    const score = min < 0 ? -1 - min : 1 - min;
    scores[id] = score;
    const outcome = score > 0 ? "W" : "L";
    const nTurns = MAX_DEPTH - Math.abs(score);
    // convert the position id to divide by 2 the disk usage
    const toLetter = (cell: string) =>
      String.fromCharCode(
        65 + 5 * (cell.charCodeAt(0) - 65) + Number(cell[1]) - 1
      );
    let str = "";
    for (let i = 0; i < 12; i += 2) {
      str += toLetter(id.substr(i, 2));
    }
    // convert the direction to a number
    const direction = [
      undefined,
      "SW",
      "S",
      "SE",
      "W",
      undefined,
      "E",
      "NW",
      "N",
      "NE",
    ].indexOf(bestMove[2]);
    // print the best move in this position
    console.log([str, bestMove[1], direction, outcome, nTurns].join(","));
  }
  // keep only the positions that have not been solved yet
  stack = newStack;
}
