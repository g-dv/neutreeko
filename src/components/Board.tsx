import React from 'react';

import { Outcome } from '../types';
import Cell from '../classes/Cell';
import Position from '../classes/Position';
import AI from '../classes/AI';

import './Board.css';

interface IProps {}

interface IState {
  selectedPawn: Cell | undefined;
  position: Position;
  currentPlayer: number;
  outcome: Outcome;
  finishedLoading: boolean;
}

export default class Board extends React.Component<IProps, IState> {
  private history: Position[];

  constructor(props: IProps) {
    super(props);
    this.state = {
      selectedPawn: undefined,
      position: new Position(),
      currentPlayer: 0,
      outcome: Outcome.Draw,
      finishedLoading: false,
    };
    this.history = [new Position()];
  }

  componentDidMount() {
    AI.init()
      .then(() => this.setState({ finishedLoading: true }))
      .catch(() => {});
  }

  select = (target: Cell) => {
    const { position, currentPlayer } = this.state;
    if (position.getPlayer(target) !== undefined) {
      if (position.getPlayer(target) === currentPlayer) {
        this.setState({
          selectedPawn: target,
        });
      } else {
        this.setState({
          selectedPawn: undefined,
        });
      }
    } else {
      if (this.isReachable(target)) {
        this.play(target);
      }
      this.setState({
        selectedPawn: undefined,
      });
    }
  };

  getClassName = (cell: Cell): string => {
    const { selectedPawn, position, currentPlayer } = this.state;
    let classes: string[];
    const player = position.getPlayer(cell);
    if (player === 0) {
      classes = ['dark-pawn', 'shadow'];
    } else if (player === 1) {
      classes = ['light-pawn', 'shadow'];
    } else {
      classes = ['empty-cell'];
    }
    if (player === currentPlayer) {
      classes.push('active');
    }
    if (selectedPawn?.equals(cell)) {
      classes.push('selected-cell');
    }
    if (this.isReachable(cell)) {
      classes.push('reachable-cell');
    }
    return classes.join(' ');
  };

  isReachable = (target: Cell): boolean => {
    const { selectedPawn, position } = this.state;
    if (!selectedPawn) {
      return false;
    }
    for (const cell of position.getReachableCells(selectedPawn as Cell)) {
      if (cell.equals(target)) {
        return true;
      }
    }
    return false;
  };

  play = (target: Cell) => {
    const { position, selectedPawn, currentPlayer } = this.state;
    position.move(selectedPawn as Cell, target);
    this.history.push(new Position(position));
    this.setState({
      currentPlayer: 1 - currentPlayer,
      outcome: AI.getOutcome(position, 1 - currentPlayer),
    });
  };

  playAsAI = () => {
    const { position, currentPlayer } = this.state;
    const result = AI.play(position, currentPlayer);
    this.history.push(new Position(result));
    this.setState({
      position: result,
      currentPlayer: 1 - currentPlayer,
      selectedPawn: undefined,
      outcome: AI.getOutcome(result, 1 - currentPlayer),
    });
  };

  cancelMove = () => {
    if (this.history.length > 1) {
      this.history.pop();
      const { currentPlayer } = this.state;
      const position = new Position(this.history[this.history.length - 1]);
      this.setState({
        position,
        currentPlayer: 1 - currentPlayer,
        selectedPawn: undefined,
        outcome: AI.getOutcome(position, 1 - currentPlayer),
      });
    }
  };

  handleDrag = (
    event: React.DragEvent<HTMLDivElement>,
    action?: 'add' | 'remove',
    argument?: string
  ) => {
    event.stopPropagation();
    event.preventDefault();
    if (action && argument) {
      (event.target as Element).classList[action](argument);
    }
  };

  getOutcomeIcon = () => {
    const { outcome } = this.state;
    if (outcome === Outcome.Victory) {
      return 'fa fa-smile';
    }
    if (outcome === Outcome.Defeat) {
      return 'fa fa-sad-tear';
    }
    return 'fa fa-question';
  };

  restart = () => {
    this.setState({
      selectedPawn: undefined,
      position: new Position(),
      currentPlayer: 0,
      outcome: Outcome.Draw,
    });
    this.history = [new Position()];
  };

  render() {
    const { currentPlayer, position, finishedLoading } = this.state;
    return (
      <div>
        <div className="board">
          <div className={!finishedLoading ? 'overlay' : 'd-none'}>
            <p>
              <i className="fa fa-circle-notch spin" />
            </p>
          </div>
          <div
            className={
              position.hasWon(1 - currentPlayer) ? 'overlay' : 'd-none'
            }
          >
            <p>{`${currentPlayer ? 'Black' : 'White'} won!`}</p>
            <button
              type="button"
              className="btn btn-light center"
              onClick={this.restart}
            >
              <i className="fa fa-undo-alt" />
            </button>
          </div>
          {[5, 4, 3, 2, 1].map((row) => (
            <div className="board-row" key={row}>
              {[1, 2, 3, 4, 5].map((col) => (
                <div
                  onMouseDown={() => this.select(new Cell(col, row))}
                  onDrop={() => this.select(new Cell(col, row))}
                  onDragEnter={(e) => this.handleDrag(e, 'add', 'hover')}
                  onDragLeave={(e) => this.handleDrag(e, 'remove', 'hover')}
                  onDragOver={(e) => this.handleDrag(e)}
                  role="button"
                  aria-label="pawn"
                  tabIndex={-1}
                  key={10 * row + col}
                  className={`board-cell ${(row + col) % 2 ? 'dark' : 'light'}`}
                >
                  <div
                    draggable={
                      currentPlayer === position.getPlayer(new Cell(col, row))
                    }
                    onDragStart={() => this.select(new Cell(col, row))}
                    className={this.getClassName(new Cell(col, row))}
                  />
                </div>
              ))}
            </div>
          ))}
        </div>
        <div className="row justify-content-between py-3 px-3">
          <button type="button" className="btn btn-secondary disabled">
            <i className={`${currentPlayer ? 'fa' : 'far'} fa-circle`} />
          </button>
          <button type="button" className="btn btn-secondary disabled">
            <i className={this.getOutcomeIcon()} />
          </button>
          <button
            type="button"
            className="btn btn-secondary"
            onClick={this.cancelMove}
            disabled={this.history.length <= 1}
          >
            <i className="fa fa-step-backward" />
          </button>
          <button
            type="button"
            className="btn btn-secondary"
            onClick={this.playAsAI}
            disabled={position.hasWon(1 - currentPlayer) || !finishedLoading}
          >
            <i className="fa fa-robot" />
          </button>
        </div>
      </div>
    );
  }
}
