# Neutreeko

Console application to play Neutreeko (http://www.neutreeko.net/neutreeko.htm) against the computer or another person.

## Possible upgrades

I made this during my free time, I may program a graphic version someday.

The AI works by using a min-max algorithm with alpha-beta cutoff. I can think of two easy ways to improve the performance :
 - By making a more advanced evaluation method. Currently, the program only looks for victory or defeat; it's not very aggressive. It could be choosing its plays depending on the chances of error of the opponent.
 - By computing in advance the opening strategies. (According to neutreeko.net, there is only one way to play the first 15 moves correctly.)

## Instructions
 
The rules can be found at http://www.neutreeko.net/neutreeko.htm.

The game settings can be found in "Settings.java".

In each round, the user input consists of three integers. The first two determine the position of the pawn to move. The third one determines the direction.

### Positions

    (0,0)  (0,1)  (0,2)  (0,3)  (0,4)  
    (1,0)  (1,1)  (1,2)  (1,3)  (1,4)  
    (2,0)  (2,1)  (2,2)  (2,3)  (2,4)  
    (3,0)  (3,1)  (3,2)  (3,3)  (3,4)  
    (4,0)  (4,1)  (4,2)  (4,3)  (4,4)

### Directions

    (7, NO) (8,  N) (9, NE)  
    (4,  O)         (6,  E)  
    (1, SO) (2,  S) (3, SE)
