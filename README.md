# Neutreeko

![Demo](./public/demo.gif)

## About

The app is hosted on Gitlab pages: [https://g-dv.gitlab.io/neutreeko](https://g-dv.gitlab.io/neutreeko).

This React implementation of the game allows you to play against your computer directly in your browser.

Neutreeko was invented by [Jan Kristian Haugland](https://www.neutreeko.net).

## Rules

The goal is to make a straight line out your 3 pawns. When moving a pawn, it must slide until stopped by an occupied square or the border of the board.

These rules are from Jean Kristian Haugland's website.

### Movement

A piece slides orthogonally or diagonally until stopped by
an occupied square or the border of the board. Black always moves first.

### Objective

To get three in a row, orthogonally or diagonally. The row must be connected.

## Strategy

There are 3,450,516 valid board positions.

If a position is not neutral and both players play perfectly, the game will end in at most 51 moves.

It is impossible to beat the AI on this website.

## Side notes

The app is completely static, which means that the calculations are all performed by your browser.

The `/java` directory is not related to the rest of this repository. It contains an implementation of the minimax algorithm for Neutreeko (which is not an efficient method to solve the game by the way). This directory is not related with the rest of this repository.
