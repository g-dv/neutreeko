export class Cell {

    public col: number;
    public row: number;

    constructor(col?: number, row?: number) {
        this.col = col;
        this.row = row;
    }

    public fromString(str: string): Cell {
        this.col = str[0].charCodeAt(0) - 64;
        this.row = Number(str[1]);
        return this;
    }

    public toString(): string {
        return 'ABCDE'.split('')[this.col - 1] + this.row;
    }

}
