export default class Cell {
  public col: number;

  public row: number;

  constructor(col = 0, row = 0) {
    this.col = col;
    this.row = row;
  }

  public equals(cell: Cell): boolean {
    return cell.col === this.col && cell.row === this.row;
  }

  public clone(): Cell {
    return new Cell(this.col, this.row);
  }

  public toString(): string {
    return 'ABCDE'.split('')[this.col - 1] + this.row;
  }
}
