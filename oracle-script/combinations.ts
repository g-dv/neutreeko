// The exported function returns every combination of k elements of arr
// Source: https://codereview.stackexchange.com/questions/200700/n-choose-k-combination-in-javascript

export function combinations(arr: any, k: any) {
  let n = arr.length;
  let result: any = [];
  let stack: any = [];
  function combine(currentNumber: any) {
    if (stack.length === k) {
      let newCombo = stack.slice();
      result.push(newCombo.map((e: any) => arr[e - 1]));
      return;
    }
    if (currentNumber > n) {
      return;
    }
    stack.push(currentNumber);
    combine(currentNumber + 1);
    stack.pop();
    combine(currentNumber + 1);
  }
  combine(1);
  return result;
}
